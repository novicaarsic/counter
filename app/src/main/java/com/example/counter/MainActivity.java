package com.example.counter;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity{

    private int num = 0;

    @SuppressLint({"SetTextI18n"})
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        TextView txtCounter = findViewById(R.id.txt_counter);
        txtCounter.setText("Counter: " + num);

        Button btnIncrement = findViewById(R.id.btn_increment);
        Button btnDecrement = findViewById(R.id.btn_decrement);
        Button btnReset= findViewById(R.id.btn_reset);

        btnIncrement.setOnClickListener(view -> {
            num++;
            txtCounter.setText("Counter: " + num);
        });

        btnDecrement.setOnClickListener(view -> {
            if (!(num < 1))
            num--;
            txtCounter.setText("Counter: " + num);
        });

        btnReset.setOnClickListener(view -> {
            num = 0;
            txtCounter.setText("Counter: " + num);
        });
    }
}